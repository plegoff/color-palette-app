Welcome to Color-Palette-App's documentation!
======================================

Color-Palette-App is a color picking / palette generating / palette saving app.

Getting started
---------------
#todo

Features
--------

- Auto generating color palettes
- Creating and fine tuning color palettes
- Saving color palettes
- Deleting color palettes
- Copy color to clipboard

Installation
------------

Environments recommendation

Development environment :
We recommend Ubuntu 18.04+, macOS OSX Lion or Windows 10

THE PROJECT IS A SIMPLE WEB APP:
 
- FRONT-END Website containing all HTML / CSS / JAVASCRIPT


libraries:

chromajs: https://gka.github.io/chroma.js/
