// Global selections and variables
const colorDivs = document.querySelectorAll('.color');
const generateBtn = document.querySelector('.generate');
const sliders = document.querySelectorAll('input[type="range"]');
const currentHexes = document.querySelectorAll('.color h2');
const popup = document.querySelector('.copy-container');
const adjustButton = document.querySelectorAll('.adjust');
const lockButton = document.querySelectorAll('.lock');
const closeAdjustments = document.querySelectorAll('.close-adjustment');
const sliderContainers = document.querySelectorAll('.sliders');
let initialColors;
// This part is for local storage
let savedPalettes = [];

// Add event listeners
generateBtn.addEventListener('click', randomColors);
sliders.forEach(slider => {
  slider.addEventListener('input', hslControls);
});
colorDivs.forEach((div,index) => {
  div.addEventListener('change', () => {
    updateTextUI(index);
  });
});
currentHexes.forEach(hex => {
  hex.addEventListener('click', () => {
    copyToClipboard(hex);
  })
});
popup.addEventListener('transitionend', () => {
  const popupBox = popup.children[0];
  popup.classList.remove('active');
  popupBox.classList.remove('active');
});
adjustButton.forEach((button, index ) => {
  button.addEventListener('click', () => {
    openAdjustmentPanel(index);
  });
});
closeAdjustments.forEach((button, index) => {
  button.addEventListener('click', () => {
    closeAdjustmentPanel(index);
  })
});
lockButton.forEach((button, index) => {
  button.addEventListener('click', (e) => {
    lockColor(index,e);
  })
});

//Functions

//Color Generator: generates string name of a random hex color
function generateHex(){
  const hexColor = chroma.random();
  return hexColor;
}

//
function randomColors(){
  //array containing initial colors ( to store the color hex and avoid transforming it into black or white when using brightness cursor)
  initialColors = [];
  colorDivs.forEach((div,index) =>{
    //div.children = h2
    const hexText = div.children[0];
    const randomColor = generateHex();

    //Add it to the initialColors array
    if(div.classList.contains('locked')) {
      // the previous one
      initialColors.push(hexText.innerText);
      return; //doesn't do anything else, ie : By putting a return at the end, we exit the loop, and move on to the next colour div.
    }else{
      initialColors.push(chroma(randomColor).hex());
    }

    //Add the color to the background
    div.style.backgroundColor = randomColor;
    hexText.innerText = randomColor;

    //Check fot contrast
    checkTextContrast(randomColor,hexText);

    //Initial colorize sliders
    const color = chroma(randomColor);
    const sliders = div.querySelectorAll('.sliders input');
    // corresponds to index 0 in the NodeList of each input
    const hue = sliders[0];
    const brightness = sliders[1];
    const saturation = sliders[2];

    colorizeSliders(color,hue,brightness,saturation);

  });
  //Reset inputs
  resetInputs();
  //Check for button contrast
  adjustButton.forEach((button, index) =>{
    checkTextContrast(initialColors[index], button);
    checkTextContrast(initialColors[index], lockButton[index]);
  });

}

function checkTextContrast(color,text){
  //Using chroma library to check luminance (from 0 to 1)
  const luminance = chroma(color).luminance();
  if(luminance > 0.5){
    text.style.color = "black";
  }else{
    text.style.color = "white";
  }
}

function colorizeSliders(color,hue,brightness,saturation){
  //Scale saturation / see: chroma.set(channel,value) -> https://gka.github.io/chroma.js/
  const noSat = color.set('hsl.s',0);
  const fullSat = color.set('hsl.s',1);
  const scaleSat = chroma.scale([noSat,color,fullSat]);
  //Scale Brightness
  //here, we know that lowest value is black and max value is white
  const midBright = color.set('hsl.l',0.5);
  const scaleBright = chroma.scale(["black",midBright,"white"]);

  //Update input colors
  saturation.style.backgroundImage = `linear-gradient(to right,${scaleSat(0)}, ${scaleSat(1)})`;
  brightness.style.backgroundImage = `linear-gradient(to right,${scaleBright(0)}, ${scaleBright(0.5)}, ${scaleBright(1)})`;
  hue.style.backgroundImage = `linear-gradient(to right, rgb(204,75,75), rgb(204,204,75), rgb(75,204,75),rgb(75,204,204), rgb(75,75,204),rgb(204,75,204), rgb(204,75,75))`;

}

function hslControls(e){
  // getting the slider index when using any of the 3 cursors
  const index =
    e.target.getAttribute('data-bright') ||
    e.target.getAttribute('data-sat') ||
    e.target.getAttribute('data-hue');

  //from input -> we go up to parent element to get slider
  let sliders = e.target.parentElement.querySelectorAll('input[type="range"]');
  const hue = sliders[0];
  const brightness = sliders[1];
  const sat = sliders[2];

  //getting background color hex value
  const bgColor = initialColors[index];
 
  let color = chroma(bgColor)
  .set('hsl.s', sat.value)
  .set('hsl.l', brightness.value)
  .set('hsl.h', hue.value);

  colorDivs[index].style.backgroundColor = color;

  //Colorize inputs/sliders
  colorizeSliders(color,hue,brightness,sat);
}

function updateTextUI(index){
  const activeDiv = colorDivs[index];
  const color = chroma(activeDiv.style.backgroundColor);
  const textHex = activeDiv.querySelector('h2');
  const icons = activeDiv.querySelectorAll('.controls button');
  textHex.innerText = color.hex();
  //check contrast
  checkTextContrast(color, textHex);
  for(icon of icons){
    checkTextContrast(color, icon)
  }
}

function resetInputs(){
  const sliders = document.querySelectorAll(".sliders input");
  sliders.forEach(slider =>{
    if(slider.name === 'hue'){
      const hueColor = initialColors[slider.getAttribute('data-hue')];
      const hueValue = chroma(hueColor).hsl()[0]; // hue here
      slider.value = Math.floor(hueValue);
    }
    if(slider.name === 'brightness'){
      const brightColor = initialColors[slider.getAttribute('data-bright')];
      const brightValue = chroma(brightColor).hsl()[2]; // brightness here
      slider.value = Math.floor(brightValue * 100) / 100;
    }
    if(slider.name === 'saturation'){
      const satColor = initialColors[slider.getAttribute('data-sat')];
      const satValue = chroma(satColor).hsl()[1]; // hue here
      slider.value = Math.floor(satValue *100) / 100;
    };
  });
};
function copyToClipboard(hex){
  //creation and destruction of a textarea to get and copy the hex (hex that's been clicked on)
  const el = document.createElement('textarea');
  el.value = hex.innerText;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
  //Pop up animation
  const popupBox = popup.children[0];
  popup.classList.add('active');
  popupBox.classList.add('active');
};
function openAdjustmentPanel(index){
  sliderContainers[index].classList.toggle("active");
};
function closeAdjustmentPanel(index){
  sliderContainers[index].classList.remove("active");
};
function lockColor(index,e){
  if(colorDivs[index].classList.toggle('locked')){
    e.target.innerHTML='<i class="fas fa-lock"><i>';
  }else{
    e.target.innerHTML='<i class="fas fa-lock-open"><i>';
  }
};

/* Separate part for SAVE TO palettes */

// Implement SAVE to palette and LOCAL STORAGE
const saveBtn = document.querySelector('.save');
const submitSave = document.querySelector('.submit-save');
const closeSave = document.querySelector('.close-save');
const saveContainer = document.querySelector('.save-container');
const saveInput = document.querySelector('.save-container input');
const libraryContainer = document.querySelector('.library-container');
const libraryBtn = document.querySelector('.library');
const closeLibraryBtn = document.querySelector('.close-library');

// Event listeners
saveBtn.addEventListener('click', openPalette);
closeSave.addEventListener('click', closePalette);
submitSave.addEventListener('click', savePalette);
libraryBtn.addEventListener('click', openLibrary);
closeLibraryBtn.addEventListener('click', closeLibrary);

// Functions
function openPalette(e){
  const popup = saveContainer.children[0];
  saveContainer.classList.add("active");
  popup.classList.add('active');
}
function closePalette(e){
  const popup = saveContainer.children[0];
  saveContainer.classList.remove("active");
  popup.classList.remove('active');
}
function savePalette(e){
  saveContainer.classList.remove('active');
  popup.classList.remove('active');
  const name = saveInput.value;
  const colors = [];
  currentHexes.forEach(hex => {
    colors.push(hex.innerText);
  });
  //Generate Object + add it to savedPalettes array
  let paletteNr;
  //this part is to make sure paletteNr is properly incremented
  const paletteObjects = JSON.parse(localStorage.getItem('palettes'));
  if(paletteObjects){
    paletteNr = paletteObjects.length;
  }else{
    paletteNr = savedPalettes.length;
  }


  const paletteObj = {name, colors, nr: paletteNr};
  savedPalettes.push(paletteObj);
  //Save to localStorage
  saveToLocal(paletteObj);
  saveInput.value = "";
  //Generate palette for library
  const palette = document.createElement('div');
  palette.classList.add('custom-palette');
  const title = document.createElement('h4');
  title.innerText = paletteObj.name;
  const preview = document.createElement('div');
  preview.classList.add('small-preview');
  paletteObj.colors.forEach(smallColor => {
    const smallDiv = document.createElement('div');
    smallDiv.style.backgroundColor = smallColor;
    preview.appendChild(smallDiv);
  });
  const paletteBtn = document.createElement('button');
  paletteBtn.classList.add('pick-palette-btn');
  paletteBtn.classList.add(paletteObj.nr);
  paletteBtn.innerText = 'Select';

  //Library palette delete button
  const paletteBtnDelete = document.createElement('button');
  paletteBtnDelete.classList.add('del-palette-btn');
  paletteBtnDelete.classList.add(paletteObj.nr);
  paletteBtnDelete.innerHTML = '<i class="fa fa-times" aria-hidden="true"></i>';

  //Attach event to the library delete buttons
  paletteBtnDelete.addEventListener('click', e => {
    
    const paletteIndex = e.target.classList[1]; //classList[1] = gives index of the palette object
    removeFromLocal(paletteIndex);
  
    resetInputs();
  });

  //Attach event to the library select buttons
  paletteBtn.addEventListener('click', e => {
    closeLibrary();
    const paletteIndex = e.target.classList[1]; //classList[1] = gives index of the palette object
    initialColors =[];
    savedPalettes[paletteIndex].colors.forEach((color,index) => {
      initialColors.push(color);
      colorDivs[index].style.backgroundColor = color;
      const text = colorDivs[index].children[0];
      checkTextContrast(color,text);
      updateTextUI(index);
    });
    resetInputs();
  });

  //Append to library
  palette.appendChild(title);
  palette.appendChild(preview);
  palette.appendChild(paletteBtn);
  palette.appendChild(paletteBtnDelete);
  libraryContainer.children[0].appendChild(palette);

};

function saveToLocal(paletteObj){
  let localPalettes;
  if(localStorage.getItem('palettes') === null){
    localPalettes = [];
  }else{
    localPalettes = JSON.parse(localStorage.getItem('palettes'));
  }
  localPalettes.push(paletteObj);
  localStorage.setItem('palettes', JSON.stringify(localPalettes));
};

//removes palette object from localStorage
function removeFromLocal(paletteIndex){
  let paletteObjects = JSON.parse(localStorage.getItem('palettes'));
  //we make a copy of paletteObjects from the localStorage
  localPalettes = [...paletteObjects];
  if (localPalettes.length === 1){
    localPalettes = [];
  }else{
    localPalettes.splice(paletteIndex,1);
  }
  localStorage.setItem('palettes', JSON.stringify(localPalettes));
  paletteObjects = JSON.parse(localStorage.getItem('palettes'));
  refreshLibrary();
};

function openLibrary() {
  const popup = libraryContainer.children[0];
  libraryContainer.classList.add('active');
  popup.classList.add('active');
};

function closeLibrary() {
  const popup = libraryContainer.children[0];
  libraryContainer.classList.remove('active');
  popup.classList.remove('active');
};
function refreshLibrary() {
  libraryContainer.children[0].innerHTML='';
  const closeLibBtn = document.createElement('button');
  closeLibBtn.classList.add('close-library');
  closeLibBtn.innerText = 'X';
  closeLibBtn.addEventListener('click', closeLibrary);

  const h4 = document.createElement('h4');
  h4.innerText = 'Pick your palette';

  libraryContainer.children[0].appendChild(closeLibBtn);
  libraryContainer.children[0].appendChild(h4);
  getLocal();
  
};

function getLocal(){
  if(localStorage.getItem('palettes') === null){
    localPalettes = [];
  }else{
    const paletteObjects = JSON.parse(localStorage.getItem('palettes'));
    //we make a copy of paletteObjects from the localStorage
    savedPalettes = [...paletteObjects];
    paletteObjects.forEach(paletteObj => {
      //Generate palette for library
      const palette = document.createElement('div');
      palette.classList.add('custom-palette');
      const title = document.createElement('h4');
      title.innerText = paletteObj.name;
      const preview = document.createElement('div');
      preview.classList.add('small-preview');
      paletteObj.colors.forEach(smallColor => {
        const smallDiv = document.createElement('div');
        smallDiv.style.backgroundColor = smallColor;
        preview.appendChild(smallDiv);
      });
      const paletteBtn = document.createElement('button');
      paletteBtn.classList.add('pick-palette-btn');
      paletteBtn.classList.add(paletteObj.nr);
      paletteBtn.innerText = 'Select';

      //Library palette delete button
      const paletteBtnDelete = document.createElement('button');
      paletteBtnDelete.classList.add('del-palette-btn');
      paletteBtnDelete.classList.add(paletteObj.nr);
      paletteBtnDelete.innerHTML = '<i class="fa fa-times" aria-hidden="true"></i>';

      //Attach event to the library delete buttons
      paletteBtnDelete.addEventListener('click', e => {
        
        const paletteIndex = e.target.classList[1]; //classList[1] = gives index of the palette object
        removeFromLocal(paletteIndex);
      
        resetInputs();
      });


      //Attach event to the library select buttons
      paletteBtn.addEventListener('click', e => {
        closeLibrary();
        const paletteIndex = e.target.classList[1]; //classList[1] = gives index of the palette object
        initialColors =[];
        paletteObjects[paletteIndex].colors.forEach((color,index) => {
          initialColors.push(color);
          colorDivs[index].style.backgroundColor = color;
          const text = colorDivs[index].children[0];
          checkTextContrast(color,text);
          updateTextUI(index);
        });
        resetInputs();
      });

      //Append to library
      palette.appendChild(title);
      palette.appendChild(preview);
      palette.appendChild(paletteBtn);
      palette.appendChild(paletteBtnDelete);
      libraryContainer.children[0].appendChild(palette);
    });
  };
};

getLocal();
randomColors();